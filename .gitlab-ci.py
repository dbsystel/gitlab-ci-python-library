from gcip import PagesJob, Pipeline, PredefinedVariables
from gcip.addons.container.sequences import build
from gcip.addons.gitlab.jobs import pages as gitlab_pages
from gcip.addons.python.jobs.build import BdistWheel
from gcip.addons.python.jobs.deploy import TwineUpload
from gcip.addons.python.jobs.linter import (
    Mypy,
    Ruff,
)
from gcip.addons.python.jobs.test import (
    EvaluateGitTagPep440Conformity,
    Pytest,
)

pipeline = Pipeline()
pipeline.initialize_image("python:3.11-slim")

pipeline.add_children(
    Ruff(),
    Pytest(),
    Mypy(package_dir="gcip"),
    # pydoc3 tries to resolve all Python objects and their attributes.
    # This includes PredefinedVariables class which are not necessarily set in every pipeline,
    # like CI_DEBUG_TRACE or CI_DEPLOY_PASSWORD . Resolving those variables lead to a KeyError.
    # When CI is unset, then PredefinedVariables behave different, returns mock values for all
    # other unset variables. That makes pdoc3 running without stumbling over those Key errors.
    gitlab_pages.Pdoc3(module="gcip", output_path="/api").prepend_scripts("unset CI"),
    gitlab_pages.AsciiDoctor(
        source="docs/index.adoc", out_file="/index.html", jobName="pages_index"
    ),
    gitlab_pages.AsciiDoctor(
        source="docs/user/index.adoc",
        out_file="/user/index.html",
        jobName="pages_user_index",
    ),
    gitlab_pages.AsciiDoctor(
        source="docs/developer/index.adoc",
        out_file="/developer/index.html",
        jobName="pages_developer_index",
    ),
    BdistWheel(),
)

fcs = build.FullContainerSequence(
    image_name="thomass/gcip",
    do_crane_push=PredefinedVariables.CI_COMMIT_TAG is not None
    or PredefinedVariables.CI_COMMIT_REF_NAME == "main",
)

fcs.dive_scan_job.set_allow_failure(True)
fcs.trivy_scan_job.set_allow_failure(True)

# gitlabci-local only works with 'sh' as kaniko and crane entrypoint
if PredefinedVariables.CI_COMMIT_REF_SLUG == "gitlab-local-sh":
    if fcs.kaniko_execute_job.image:
        fcs.kaniko_execute_job.image.entrypoint = ["sh"]
    if fcs.crane_push_job and fcs.crane_push_job.image:
        fcs.crane_push_job.image.entrypoint = ["sh"]

pipeline.add_children(fcs, name="gcip")

if PredefinedVariables.CI_COMMIT_TAG:
    pipeline.add_children(
        EvaluateGitTagPep440Conformity(),
        TwineUpload(),
    )

pipeline.add_children(PagesJob())
pipeline.write_yaml()
