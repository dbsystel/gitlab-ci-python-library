import os

import tomli
from setuptools import find_packages, setup

__author__ = "Thomas Steinbach"
__copyright__ = "Copyright 2020 DB Systel GmbH"
__credits__ = ["Thomas Steinbach"]
# SPDX-License-Identifier: Apache-2.0
__license__ = "Apache-2.0"
__maintainer__ = "Thomas Steinbach"
__email__ = "thomas.t.steinbach@deutschebahn.com"

with open("README.md") as fp:
    long_description_from_readme = fp.read()

with open("Pipfile", "rb") as pipfile:
    """
    Turns the `packages` section of the Pipfile into a list like ["package1~=1.2", "package2>=3.4", ...]
    """
    install_requires = [
        key + value for key, value in tomli.load(pipfile)["packages"].items()
    ]

setup(
    name="gcip",
    setuptools_git_versioning={"enabled": True},
    description="The Gitlab CI Python Library",
    long_description=long_description_from_readme,
    long_description_content_type="text/markdown",
    author="Thomas Steinbach",
    author_email="thomas.t.steinbach@deutschebahn.com",
    url=os.getenv(
        "CI_PROJECT_URL",
        "only available when package was build in a Gitlab CI pipeline",
    ),
    packages=find_packages(exclude=("tests*",)),
    include_package_data=True,
    python_requires="~=3.11",
    install_requires=install_requires,
)
