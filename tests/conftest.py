import inspect
import os
import pathlib
from typing import Any, Dict

import pytest
import yaml

from gcip.addons.container.config import DockerClientConfig
from gcip.core.job import Job
from gcip.core.pipeline import Pipeline


def check(output: Dict[str, Any]) -> None:
    yaml_output = yaml.safe_dump(output, default_flow_style=False, sort_keys=False)
    # inspired by https://stackoverflow.com/a/60297932
    caller_file_path, caller_file_name = os.path.split(
        os.path.abspath(inspect.stack()[1].filename)
    )
    caller_file_name = os.path.splitext(caller_file_name)[0]
    caller_function_name = inspect.stack()[1].function
    compare_file = f"{caller_file_path}/comparison_files/{caller_file_name}_{caller_function_name}.yml"

    if os.getenv("UPDATE_TEST_OUTPUT", "false").lower() == "true":
        pathlib.Path(os.path.split(compare_file)[0]).mkdir(parents=True, exist_ok=True)
        with open(compare_file, "w") as outfile:
            outfile.write(yaml_output)
    else:
        try:
            with open(compare_file, "r") as infile:
                assert yaml_output == infile.read()
        except FileNotFoundError as exc:
            print(
                "Comparison file not found.",
                "Create it by executing:\n\n",
                f"\tUPDATE_TEST_OUTPUT=true pytest {caller_file_path}",
            )
            raise exc
        except AssertionError as exc:
            print(
                "If intentionally, you can update the comparions file:\n\n",
                "\trm -rf test/unit/comparison_files/*; UPDATE_TEST_OUTPUT=true pytest\n\n",
                "Always review the results carefully with 'git diff'!",
            )
            raise exc


@pytest.fixture
def pipeline() -> Pipeline:
    """
    Returns:
        Pipeline: A new `Pipeline()` object.
    """
    return Pipeline()


@pytest.fixture
def job_foo(pipeline) -> Job:
    """
    Returns:
        Job: A new `Job(stage="foo", script="foo")` object already added to the `pipeline` fixture.
    """
    job = Job(name="foo", script="foo")
    pipeline.add_children(job)
    return job


@pytest.fixture
def job_bar(pipeline) -> Job:
    """
    Returns:
        Job: A new `Job(stage="bar", script="bar")` object already added to the `pipeline` fixture.
    """
    job = Job(name="bar", script="bar")
    pipeline.add_children(job)
    return job


@pytest.fixture()
def docker_client_config() -> DockerClientConfig:
    dcc = DockerClientConfig()
    dcc.add_auth(registry="index.docker.io")
    dcc.add_cred_helper(
        "0132456789.dkr.eu-central-1.amazonaws.com", cred_helper="ecr-login"
    )
    return dcc
