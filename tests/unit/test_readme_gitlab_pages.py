from gcip import PagesJob, Pipeline
from gcip.addons.gitlab.jobs.pages import AsciiDoctor
from tests import conftest


def test():
    pipeline = Pipeline()
    pipeline.add_children(
        AsciiDoctor(source="docs/index.adoc", out_file="/index.html"),
        PagesJob(),
    )

    conftest.check(pipeline.render())
