from gcip.addons.container.config import DockerClientConfig
from gcip.addons.container.registries import Registry
from gcip.addons.container.sequences.copy import (
    CopyContainer,
)
from gcip.core.pipeline import Pipeline
from tests import conftest


def test_addons_container_sequences_copy_container():
    pipeline = Pipeline()
    aws_registry = Registry.AWS(account_id="1234567890123", region="eu-central-1")
    dcc = DockerClientConfig()
    dcc.add_auth(Registry.DOCKER)
    dcc.add_auth(aws_registry, "ecr-login")
    s = CopyContainer(
        src_registry=Registry.DOCKER,
        dst_registry=aws_registry,
        image_name="gcip",
        image_tag="0.10.0",
    )
    s.trivy_scan_job.set_image("custom/trivy:v1.2.3")
    s.crane_pull_job.docker_client_config = dcc
    s.crane_push_job.docker_client_config = dcc
    pipeline.add_children(s, stage="container")
    conftest.check(pipeline.render())


def test_addons_container_sequences_copy_container_without_checks():
    pipeline = Pipeline()
    s = CopyContainer(
        src_registry=Registry.DOCKER,
        dst_registry=Registry.QUAY,
        image_name="busybox",
        image_tag="latest",
        do_dive_scan=False,
        do_trivy_scan=False,
    )
    s.crane_pull_job.set_image("custom/crane:1.2.1")
    s.crane_push_job.set_image("custom/crane:1.2.1")
    pipeline.add_children(s)
    conftest.check(pipeline.render())
