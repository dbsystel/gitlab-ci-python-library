from gcip import Pipeline
from gcip.addons.aws.sequences.cdk import DiffDeploy
from tests import conftest


def test_diff_deploy_multiple_stacks() -> None:
    pipeline = Pipeline()
    s = DiffDeploy(stacks=["stack1", "stack2"])
    s.deploy_job.toolkit_stack_name = "toolkit-stack"
    pipeline.add_children(s)
    conftest.check(pipeline.render())


def test_diff_deploy_with_context() -> None:
    pipeline = Pipeline()
    s = DiffDeploy(
        stacks=["teststack"],
        context={"foo": "bar", "abra": "kadabra"},
    )
    s.deploy_job.toolkit_stack_name = "toolkit-stack"
    pipeline.add_children(s)
    conftest.check(pipeline.render())


def test_diff_deploy_with_options() -> None:
    pipeline = Pipeline()
    s = DiffDeploy(stacks=["teststack"])
    s.diff_job.diff_options = "-d"
    s.deploy_job.deploy_options = "-e"
    s.deploy_job.toolkit_stack_name = "toolkit-stack"
    pipeline.add_children(s)
    conftest.check(pipeline.render())


def test_diff_deploy_without_pip_install() -> None:
    pipeline = Pipeline()
    s = DiffDeploy(stacks=["teststack"])
    s.deploy_job.install_gcip = False
    pipeline.add_children(s)
    conftest.check(pipeline.render())
