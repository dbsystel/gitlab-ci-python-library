import pytest

from gcip import Job, Pipeline, PredefinedVariables
from tests import conftest


@pytest.fixture
def testjob():
    return Job(stage="testjob", script="foobar")


def test_init_empty_variables(testjob):
    pipeline = Pipeline()
    pipeline.initialize_variables(variable1="foo", variable2="bar")
    pipeline.add_children(testjob)
    conftest.check(pipeline.render())


def test_init_non_empty_variables(testjob):
    pipeline = Pipeline()
    pipeline.initialize_variables(variable1="foo", variable2="bar")
    testjob.add_variables(variable1="keep", variable2="those", variable3="variables")
    pipeline.add_children(testjob)
    conftest.check(pipeline.render())


def test_override_variables(testjob):
    pipeline = Pipeline()
    pipeline.override_variables(variable1="new", variable2="values")
    testjob.add_variables(variable1="replace", variable2="those", variable3="variables")
    pipeline.add_children(testjob)
    conftest.check(pipeline.render())


def test_predefined_variables_in_pipeline_env():
    assert PredefinedVariables.CI == "true"
    assert PredefinedVariables.CI_PROJECT_NAME == "gitlab-ci-project"


def test_predefined_variables_non_pipeline_env(monkeypatch):
    monkeypatch.delenv("CI", raising=False)

    # If env var CI is not set all EnvProxy varaibles returning
    # notRunningInAPipeline except if they are initialised by
    # the calling scope
    monkeypatch.delenv("CHAT_CHANNEL", raising=False)
    assert PredefinedVariables.CHAT_CHANNEL == "notRunningInAPipeline"
    # Some of the PredefinedVariables are of type OptionalEnvProxy means,
    # they will return None instead of KeyError exception.
    monkeypatch.delenv("CI_COMMIT_BRANCH", raising=False)
    assert PredefinedVariables.CI_COMMIT_BRANCH is None


def test_sensitive_variables_are_unresolved():
    assert (
        PredefinedVariables.CI_DEPENDENCY_PROXY_PASSWORD
        == "${CI_DEPENDENCY_PROXY_PASSWORD}"
    )
    assert PredefinedVariables.CI_DEPLOY_PASSWORD == "${CI_DEPLOY_PASSWORD}"
    assert PredefinedVariables.CI_JOB_TOKEN == "${CI_JOB_TOKEN}"
    assert PredefinedVariables.CI_JOB_JWT == "${CI_JOB_JWT}"
    assert PredefinedVariables.CI_REGISTRY_PASSWORD == "${CI_REGISTRY_PASSWORD}"
    assert PredefinedVariables.CI_REPOSITORY_URL == "${CI_REPOSITORY_URL}"
    assert PredefinedVariables.CI_RUNNER_SHORT_TOKEN == "${CI_RUNNER_SHORT_TOKEN}"
