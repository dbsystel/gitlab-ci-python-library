from gcip import Job, Pipeline, Rule
from tests import conftest


def test():
    pipeline = Pipeline()
    job = Job(stage="print_date", script="date")
    job.set_image("docker/image:example")
    job.prepend_scripts("./before-script.sh")
    job.append_scripts("./after-script.sh")
    job.add_variables(USER="Max Power", URL="https://example.com")
    job.add_tags("test", "europe")
    job.append_rules(Rule(if_statement="$MY_VARIABLE_IS_PRESENT"))
    job.artifacts.add_paths("binaries/", ".config")
    pipeline.add_children(job)

    conftest.check(pipeline.render())
