import tempfile

import yaml

import gcip
from gcip import (
    IncludeLocal,
    IncludeRemote,
    IncludeTemplate,
    Job,
    Service,
)
from gcip.addons.aws.sequences.cdk import DiffDeploy
from gcip.addons.gitlab.scripts import clone_repository
from gcip.core.pipeline import Pipeline
from gcip.lib import rules
from tests import conftest


def myapp_diff_deploy(environment: str, resource: str) -> gcip.Sequence:
    sequence = DiffDeploy(stacks=[f"myapp-{environment}-{resource}"])
    sequence.deploy_job.toolkit_stack_name = f"application-{environment}-cdk-toolkit"
    return sequence


def environment_pipeline(environment: str) -> gcip.Sequence:
    env_pipe = gcip.Sequence()

    env_pipe.add_children(
        myapp_diff_deploy(environment, "project-resources"), stage="project_resources"
    )

    if environment == "unstable":
        env_pipe.add_children(
            myapp_diff_deploy(environment, "windows-vm-bucket"),
            stage="windows_vm_bucket",
        )
        update_image_job = gcip.Job(
            stage="update-windows-vm-image",
            script=f"python3 update_windows_vm_image.py {environment}",
        )
        update_image_job.append_rules(
            rules.on_merge_request_events().never(),
            gcip.Rule(if_statement="$IMAGE_SOURCE_PASSWORD"),
        )
        env_pipe.add_children(update_image_job)
    else:
        env_pipe.add_children(
            gcip.Job(
                stage="copy-windows-vm-image",
                script=f"python3 update_windows_vm_image.py {environment}",
            )
        )

    if environment == "dev":
        env_pipe.add_children(
            myapp_diff_deploy(environment, "windows-vm-instances-barista"),
            stage="windows_vm_intances",
            name="barista",
        )
        env_pipe.add_children(
            myapp_diff_deploy(environment, "windows-vm-instances-impala"),
            stage="windows_vm_intances",
            name="impala",
        )
    else:
        env_pipe.add_children(
            myapp_diff_deploy(environment, "windows-vm-instances"),
            stage="windows_vm_intances",
        )

    return env_pipe


def test_full_pipeline_yaml_output():
    pipeline = gcip.Pipeline()
    pipeline.initialize_image("python:3.11-slim")
    pipeline.prepend_scripts(
        clone_repository("otherproject/configuration"),
        "./install-dependencies.sh",
    )
    pipeline.add_tags("environment-iat")

    for environment in ["unstable", "dev", "tst", "iat"]:
        env_pipe = environment_pipeline(environment)
        if environment == "unstable":
            env_pipe.add_variables(MYPROJECT_RELEASE_VERSION=">=0.dev")
        else:
            env_pipe.add_variables(MYPROJECT_RELEASE_VERSION="==0.0.dev10")
        pipeline.add_children(env_pipe, stage=environment)

    conftest.check(pipeline.render())


def test_includes_pipeline():
    first_include = IncludeLocal("local-file.yml")
    second_include = IncludeRemote("https://gitlab.com/my/project/include_file.yml")
    pipeline = gcip.Pipeline(includes=[first_include, second_include])
    pipeline.add_include(IncludeTemplate("Template-Include-File.yml"))
    conftest.check(pipeline.render())


def test_write_yaml():
    pipeline = gcip.Pipeline()
    pipeline.add_children(gcip.Job(script="testjob", stage="test"))
    target = tempfile.TemporaryFile()
    pipeline.write_yaml(target.name)


def test_services():
    pipeline = gcip.Pipeline()
    pipeline.add_services("foo", "bar")
    pipeline.add_services(Service("baz"))
    conftest.check(pipeline.render())


def test_pipeline_with_contextmanager():
    with Pipeline() as pipeline:
        pipeline.add_children(Job(script="echo 'I am a simple job'", name="simple_job"))

    with open("generated-config.yml", "r") as config:
        pipeline_obj = yaml.safe_load(config)
    assert pipeline_obj["stages"] == ["test"]
    assert pipeline_obj["simple-job"] == {
        "stage": "test",
        "script": ["echo 'I am a simple job'"],
    }
