from gcip import Image
from gcip.addons.container.images import PredefinedImages


def test_image_class_only_with_image():
    image = Image("alpine:3")
    assert image.name == "alpine:3"
    assert image.entrypoint is None
    assert image.render() == {
        "name": "alpine:3",
    }


def test_image_class_with_entrypoint():
    image = Image("alpine:3", entrypoint=["/bin/sh", "-c", "cat", "/etc/os-release"])
    assert image.name == "alpine:3"
    assert image.entrypoint == ["/bin/sh", "-c", "cat", "/etc/os-release"]
    assert image.render() == {
        "name": "alpine:3",
        "entrypoint": ["/bin/sh", "-c", "cat", "/etc/os-release"],
    }


def test_image_with_tag():
    assert PredefinedImages.GCIP.with_tag("otherThanLatest").tag == "otherThanLatest"
    assert PredefinedImages.GCIP.tag == "latest"


def test_image_with_entrypoint():
    assert PredefinedImages.GCIP.with_entrypoint("foo", "bar").entrypoint == [
        "foo",
        "bar",
    ]
    assert PredefinedImages.GCIP.entrypoint is None
