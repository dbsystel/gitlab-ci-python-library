from gcip.addons.aws.jobs import codecommit
from tests import conftest


def test_mirror_job(pipeline):
    pipeline.add_children(codecommit.MirrorToCodecommit())
    conftest.check(pipeline.render())


def test_mirror_job_configured(pipeline):
    pipeline.add_children(
        codecommit.MirrorToCodecommit(
            repository_name="testrepo",
            infrastructure_tags="foo=bar,max=muster",
        )
    )
    conftest.check(pipeline.render())
