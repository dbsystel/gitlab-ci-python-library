from gcip import Pipeline
from gcip.addons.container.jobs.docker import Build, Push
from tests import conftest


def test_default_docker_jobs():
    pipeline = Pipeline()

    pipeline.add_children(
        Build(repository="myspace/myimage"),
        Push(container_image="myspace/myimage"),
    )

    conftest.check(pipeline.render())
