import pytest

from gcip import Job, Need, Pipeline, Sequence
from gcip.core.variables import PredefinedVariables
from tests import conftest


@pytest.fixture
def testjob():
    return Job(stage="testjob", script="foobar")


def test_simple_need():
    conftest.check(Need("testjob").render())


def test_no_artifacts():
    conftest.check(Need("testjob", artifacts=False).render())


def test_other_project_need():
    conftest.check(Need("testjob", project="foo/bar").render())


def test_other_project_ref_need():
    conftest.check(Need("testjob", project="foo/bar", ref="test").render())


def test_job_with_needs(testjob):
    job = Job(stage="depending_job", script="bar")
    job.add_needs(testjob, Need("job1"), Need("job2", project="foo/bar"))
    conftest.check(Pipeline().add_children(testjob, job).render())


def test_sequence_with_needs(testjob):
    sequence = Sequence()
    pipeline = Pipeline()
    pipeline.add_children(testjob).add_children(sequence)
    sequence.add_children(
        Job(stage="firstjob", script="foo"), Job(stage="secondjob", script="bar")
    )
    sequence.add_needs(testjob, Need("job1"), Need("job2"))
    conftest.check(pipeline.render())


def test_sequence_with_parallel_jobs_and_needs(testjob):
    sequence = Sequence()
    pipeline = Pipeline()
    pipeline.add_children(testjob).add_children(sequence)
    sequence.add_children(
        Job(stage="job", name="first", script="foo"),
        Job(stage="secondjob", script="bar"),
        Job(stage="job", name="third", script="baz"),
        Job(stage="fourthjob", script="maz"),
    )
    sequence.add_needs(testjob)
    conftest.check(pipeline.render())


def test_add_sequence_as_need(testjob):
    sequence = Sequence()
    sequence.add_children(
        Job(stage="first", name="A", script="firstDateA"),
        Job(stage="second", name="A", script="secondDateA"),
        Job(stage="last", name="A", script="lastDateA"),
        Job(stage="second", name="B", script="secondDateB"),
        Job(stage="last", name="B", script="lastDateB"),
        Job(stage="first", name="B", script="firstDateB"),
    )

    testjob.add_needs(sequence)

    pipeline = Pipeline()
    pipeline.add_children(testjob)
    conftest.check(pipeline.render())


def test_needs_will_be_staged():
    job1 = Job(stage="first", script="foobar")
    sequence = Sequence().add_children(
        Job(stage="second", script="foobar"), stage="SSS"
    )

    targetJob = Job(stage="target1", script="foobar").add_needs(job1, sequence)
    targetSequence = (
        Sequence()
        .add_children(Job(stage="target2", script="foobar"), stage="TTT")
        .add_needs(job1, sequence)
    )

    sequenceWithoutStage = Sequence()
    sequenceWithoutStage.add_children(job1)
    sequenceWithoutStage.add_children(sequence)

    parentSequence = Sequence().add_children(sequenceWithoutStage, stage="abc")

    parentSequence2 = Sequence()
    parentSequence2.add_children(targetJob, stage="xyz")
    parentSequence2.add_children(targetSequence, stage="xyz")

    parentParentSequence = Sequence().add_children(parentSequence, stage="123")

    pipeline = Pipeline().add_children(
        parentParentSequence, parentSequence2, stage="final"
    )
    conftest.check(pipeline.render())


def test_needs_sequence_with_stage_only():
    """
    In a former version of gcip, adding jobs to a sequence with providing `stage`
    leads to a need reference name with a `-` at the end:

    ```
    job2-stage-job-seq1:
      needs:
      - job: job1-stage-job-seq1-
    ```
    """
    pipeline = Pipeline()
    job1 = Job(script='echo "I\'m job1"', name="job", stage="job1_stage")
    job2 = Job(script='echo "I\'m job2"', name="job", stage="job2_stage")
    job2.add_needs(job1)

    seq1 = Sequence()
    seq1.add_children(job1, job2, stage="seq1")
    pipeline.add_children(seq1)
    conftest.check(pipeline.render())


def test_need_pipeline(pipeline, job_foo, job_bar):
    job_foo.add_needs(Need("test", pipeline="567890"))
    job_bar.add_needs(Need(pipeline="other/project"))
    conftest.check(pipeline.render())


def test_no_job_and_pipeline_set():
    with pytest.raises(
        ValueError, match="At least one of `job` or `pipeline` must be set."
    ):
        Need(project="foobar")


def test_project_and_pipeline_raises_error():
    with pytest.raises(ValueError, match="Needs accepts either"):
        Need("test", project="foo", pipeline="bar")


def test_fail_on_pipeline_is_ci_project_id():
    with pytest.raises(
        ValueError, match="The pipeline attribute does not accept the current pipeline"
    ):
        Need("test", pipeline=PredefinedVariables.CI_PIPELINE_ID)


def test_need_name_generation():
    pipeline = Pipeline()

    # First sequence with normal jobs
    # Job2 depends on Job1
    seq1 = Sequence()
    job1 = Job(script="date", name="job1name", stage="job1stage")
    job2 = Job(script="date", name="job2name", stage="job2stage")
    seq1.add_children(job1, job2.add_needs(job1), name="seq1name", stage="seq1stage")

    # Second sequence
    # This sequence gets sequence1 with a custom name
    seq2 = Sequence()
    seq2.add_children(seq1, name="seq2name", stage="seq2stage")
    pipeline.add_children(seq2, name="pipename", stage="pipestage")

    rendered_pipeline = pipeline.render()
    assert (
        "pipename-pipestage-seq2name-seq2stage-seq1name-seq1stage-job1name-job1stage"
        == rendered_pipeline[
            "pipename-pipestage-seq2name-seq2stage-seq1name-seq1stage-job2name-job2stage"
        ]["needs"][0]["job"]
    )


def test_empty_needs():
    pipeline = Pipeline()
    pipeline.add_children(
        Job(script="date", name="job1name", stage="job1stage").set_needs([])
    )
    conftest.check(pipeline.render())
