from gcip.addons.notification.jobs import mail
from tests import conftest


def test_sendmail_job(pipeline):
    pipeline.add_children(mail.SendMail())
    conftest.check(pipeline.render())


def test_sendmail_job_custom_vars(pipeline):
    pipeline.add_children(
        mail.SendMail(
            smtpServerVar="PYTEST_SERVER",
            smtpPortVar="PYTEST_PORT",
            smtpUserVar="PYTEST_USER",
            smtpPasswordVar="PYTEST_PASSWORD",
            fromEmailVar="PYTEST_FROM",
            toEmailsVar="PYTEST_TO",
            subjectVar="PYTEST_SUBJECT",
            emailBodyPathVar="PYTEST_BODY",
        )
    )
    conftest.check(pipeline.render())


def test_sendmail_job_parameterized(pipeline):
    pipeline.add_children(
        mail.SendMail(
            smtpServer="mail.server",
            smtpPort=1234,
            smtpUser="noman",
            fromEmail="noman@mail.server",
            toEmails="nobody@void.net",
            subject="pytest",
            emailBodyPath="/path/to/file",
        )
    )
    conftest.check(pipeline.render())


def test_sendmail_job_with_mail_body(pipeline):
    pipeline.add_children(
        mail.SendMail(
            emailBody="""A multiline body
with ${CI_PIPELINE_URL} included.
Works.""",
        )
    )
    conftest.check(pipeline.render())
