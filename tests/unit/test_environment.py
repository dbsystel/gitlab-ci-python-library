from gcip import Environment


def test_environment_class_only_with_name():
    environment = Environment("environment_name")
    assert environment.name == "environment_name"
    assert environment.url is None
    assert environment.render() == {
        "name": "environment_name",
    }


def test_environment_class_with_url():
    environment = Environment("environment_name", url="https://environment_name")
    assert environment.name == "environment_name"
    assert environment.url == "https://environment_name"
    assert environment.render() == {
        "name": "environment_name",
        "url": "https://environment_name",
    }


def test_job_with_url():
    environment = Environment("environment_name")
    assert (
        environment.with_url("https://environment_name").url
        == "https://environment_name"
    )
    assert environment.url is None
