from gcip.addons.security.sops import (
    sops_export_decrypted_values,
)


def test_sops_export_decrypted_values():
    expected = [
        "set -eo pipefail; SOPS_OUTPUT=$(sops -d secrets/encrypted_file.env); export $SOPS_OUTPUT"
    ]
    assert (
        sops_export_decrypted_values("secrets/encrypted_file.env", install_sops=False)
        == expected
    )


def test_sops_export_decrypted_values_with_download():
    expected = [
        "curl -L https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v.3.7.1.linux -o /usr/local/bin/sops",
        "chmod +x /usr/local/bin/sops",
        "set -eo pipefail; SOPS_OUTPUT=$(sops -d secrets/encrypted_file.env); export $SOPS_OUTPUT",
    ]
    assert sops_export_decrypted_values("secrets/encrypted_file.env") == expected
