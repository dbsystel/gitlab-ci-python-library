import gcip
from gcip.addons.python.jobs.linter import Flake8
from tests import conftest


def test():
    pipeline = gcip.Pipeline()
    pipeline.add_children(Flake8())

    conftest.check(pipeline.render())
