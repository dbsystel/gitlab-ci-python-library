from gcip import Pipeline
from gcip.addons.container.jobs.crane import (
    Copy,
    Pull,
    Push,
)
from gcip.addons.container.registries import Registry
from gcip.core.image import Image
from tests import conftest


def test_simple_crane_copy_job():
    pipeline = Pipeline()

    pipeline.add_children(
        Copy(
            src_registry="index.docker.io",
            src_repository="alpine",
            src_tag="3",
            dst_repository="user/alpine",
        ),
        name="default",
    )
    pipeline.add_children(
        Copy(
            src_registry="quay.io",
            src_repository="wagoodman/dive",
            src_tag="0.10.0",
            dst_registry="index.docker.io",
            dst_repository="user/dive",
            dst_tag="latest",
        ).set_image(Image("index.docker.io/user/crane:latest")),
        name="custom_image",
    )
    conftest.check(pipeline.render())


def test_advanced_crane_copy_job(docker_client_config):
    pipeline = Pipeline()
    pipeline.add_children(
        Copy(
            src_registry="index.docker.io",
            src_repository="alpine",
            src_tag="3",
            dst_registry="0132456789.dkr.eu-central-1.amazonaws.com",
            dst_repository="namespace/alpine",
            docker_client_config=docker_client_config,
        ),
        name="with_authentication",
    )
    conftest.check(pipeline.render())


def test_simple_crane_push_job():
    pipeline = Pipeline()
    pipeline.add_children(Push(dst_registry="index.docker.io"), name="push_image")
    conftest.check(pipeline.render())


def test_advanced_crane_push_job(docker_client_config):
    pipeline = Pipeline()
    pipeline.add_children(
        Push(
            dst_registry="index.docker.io",
            image_name="crane",
            docker_client_config=docker_client_config,
        ).set_image("crane_image:v1.1.2"),
        name="push_image",
    )
    conftest.check(pipeline.render())


def test_crane_push_on_main(monkeypatch):
    monkeypatch.setenv("CI_COMMIT_REF_NAME", "main")
    monkeypatch.delenv("CI_COMMIT_TAG")
    pipeline = Pipeline()
    pipeline.add_children(
        Push(dst_registry=Registry.DOCKER),
        name="push_image_with_latest_tag",
    )
    conftest.check(pipeline.render())


def test_addons_container_jobs_crane_push_registry(docker_client_config):
    pipeline = Pipeline()
    pipeline.add_children(
        Push(
            dst_registry=Registry.DOCKER,
            image_name="crane",
            docker_client_config=docker_client_config,
        ).set_image("crane_image:v1.1.2"),
        name="push_image",
    )
    conftest.check(pipeline.render())


def test_crane_simple_pull():
    pipeline = Pipeline()
    pipeline.add_children(
        Pull(
            src_registry=Registry.GCR,
            image_name="awsome/image",
        )
    )
    conftest.check(pipeline.render())


def test_crane_advanced_pull(docker_client_config):
    pipeline = Pipeline()
    pipeline.add_children(
        Pull(
            src_registry=Registry.GCR,
            docker_client_config=docker_client_config,
            image_name="thomass/gcip",
            image_tag="main",
            tar_path="test/foo/bar",
        )
    )
    conftest.check(pipeline.render())
