from gcip.addons.gitlab.jobs.pages import (
    AsciiDoctor,
    Pdoc3,
    Sphinx,
)
from tests import conftest


def test_asciidoctor(pipeline):
    pipeline.add_children(
        AsciiDoctor(source="docs/index.adoc", out_file="/index.html"),
        AsciiDoctor(
            source="docs/awesome.adoc",
            out_file="/awesome.html",
            jobName="pages_awesome",
        ),
    )
    conftest.check(pipeline.render())


def test_pages_pdoc3(pipeline):
    pipeline.add_children(
        Pdoc3(module="gcip"),
        Pdoc3(module="userdoc", output_path="/user", jobName="userdoc"),
    )
    conftest.check(pipeline.render())


def test_pages_sphinx(pipeline):
    pipeline.add_children(Sphinx())
    conftest.check(pipeline.render())
