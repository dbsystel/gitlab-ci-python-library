import pytest

from gcip.addons.aws.jobs.cdk import Bootstrap, Deploy, Diff, WaitForStackReady
from tests import conftest


def test_bootstrap(pipeline) -> None:
    pipeline.add_children(
        Bootstrap(
            aws_account_id="1234567890",
            aws_region="net-wunderland-1",
            toolkit_stack_name="my-cdk-toolkit-dev",
            qualifier="beautifulapp",
        ),
        stage="dev",
    )
    pipeline.add_children(
        Bootstrap(
            aws_account_id="1234567890",
            aws_region="net-wunderland-1",
            toolkit_stack_name="my-cdk-toolkit-tst",
            qualifier="beautifulapp",
            resource_tags={
                "ApplicationName": "testapp",
                "Subsystem": "testsystem",
            },
        ),
        stage="tst",
    )

    conftest.check(pipeline.render())


def test_diff_deploy_with_context(pipeline) -> None:
    pipeline.add_children(
        Diff(stacks=["teststack"], context={"foo": "bar", "abra": "kadabra"}),
        Deploy(
            stacks=["teststack"],
            toolkit_stack_name="CDKToolkit",
            context={"foo": "bar", "abra": "kadabra"},
        ),
    )

    conftest.check(pipeline.render())


def test_deploy_with_assume_role(pipeline) -> None:
    pipeline.add_children(
        Deploy(
            stacks=["teststack"],
            toolkit_stack_name="CDKToolkit",
        ),
        stage="local-role",
    )
    pipeline.add_children(
        Deploy(
            stacks=["teststack"],
            toolkit_stack_name="CDKToolkit",
        ),
        stage="remote-role",
    )
    conftest.check(pipeline.render())


def test_options(pipeline) -> None:
    pipeline.add_children(
        Diff(stacks=["teststack"], diff_options="-o"),
        Deploy(
            stacks=["teststack"], toolkit_stack_name="CDKToolkit", deploy_options="-i"
        ),
    )
    conftest.check(pipeline.render())


def test_wait_for_stack(pipeline):
    pipeline.add_children(
        WaitForStackReady(
            stacks=["teststack"],
        )
    )
    conftest.check(pipeline.render())


def test_wait_for_stack_custom(pipeline):
    pipeline.add_children(
        WaitForStackReady(
            stacks=["teststack"],
            wait_for_stack_account_id="1234567890",
            wait_for_stack_assume_role="MasterOfDesaster",
        )
    )


def test_assume_role_warning(pipeline) -> None:
    with pytest.warns(
        UserWarning,
        match="`wait_for_stack_account_id` has no effects without `wait_for_stack_assume_role`",
    ):
        pipeline.add_children(
            WaitForStackReady(
                stacks=["teststack"],
                wait_for_stack_account_id="MasterOfDesaster",
            )
        )
        pipeline.render()
