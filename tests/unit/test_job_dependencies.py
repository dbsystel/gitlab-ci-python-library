from gcip.core.job import Job
from gcip.core.pipeline import Pipeline
from gcip.core.sequence import Sequence
from tests import conftest


def test_dependencies(pipeline: Pipeline):
    job1 = Job(name="job1", script="date")
    job2 = Job(name="job2", script="date")
    job2.add_dependencies(job1)

    job3 = Job(name="job3", script="date")
    job4 = Job(name="job4", script="date")
    sequence = Sequence().add_children(job3, job4, name="in", stage="sequence")
    job5 = Job(name="job5", script="date")
    job5.add_dependencies(sequence)

    seq2 = Sequence().add_children(sequence)
    seq3 = Sequence().add_children(seq2, job5, name="bar")

    job6 = Job(name="job6", script="date")
    job6.add_dependencies(job1, job2, job3, job4, job5, seq3)

    pipeline.add_children(job1, job2, seq3, job6)
    conftest.check(pipeline.render())
