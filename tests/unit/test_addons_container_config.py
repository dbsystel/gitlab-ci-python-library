from gcip.addons.container.config import DockerClientConfig
from gcip.addons.container.registries import Registry
from tests import conftest


def test_docker_client_config():
    dcc = DockerClientConfig()
    dcc.add_auth("index.docker.com")
    conftest.check({"shell_command": dcc.get_shell_command()})


def test_complex_docker_client_config():
    dcc = DockerClientConfig()
    dcc.add_auth("index.docker.com", "CUSTOM_DOCKER_USER", "CUSTOM_DOCKER_PW")
    dcc.add_cred_helper("123456789.dkr.ecr.eu-central-1.amazonaws.com", "ecr-login")
    dcc.set_creds_store("gcr")
    dcc.add_raw({"proxies": {"default": {"httpProxy": "127.0.0.1:1234"}}})
    conftest.check({"shell_command": dcc.get_shell_command()})


def test_docker_client_config_set_config_file_path():
    dcc = DockerClientConfig()
    dcc.set_config_file_path("/kaniko/.docker/other.json")
    conftest.check({"shell_command": dcc.get_shell_command()})


def test_docker_client_config_registry_constant():
    dcc = DockerClientConfig()
    dcc.add_auth(Registry.DOCKER)
    dcc.add_cred_helper(Registry.GCR, cred_helper="gcr-login")
    conftest.check({"shell_command": dcc.get_shell_command()})
