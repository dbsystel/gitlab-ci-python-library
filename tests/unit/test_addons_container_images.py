import pytest

from gcip.addons.container.images import PredefinedImages


@pytest.mark.parametrize(
    "image,name,tag,entrypoint",
    [
        ("KANIKO", "gcr.io/kaniko-project/executor", "debug", [""]),
        ("CRANE", "gcr.io/go-containerregistry/crane", "debug", [""]),
        ("DIVE", "wagoodman/dive", "latest", [""]),
        ("GCIP", "thomass/gcip", "latest", None),
        ("TRIVY", "aquasec/trivy", "latest", [""]),
        ("BUSYBOX", "busybox", "latest", None),
    ],
)
def test_predefined_images(image, name, tag, entrypoint):
    test_image = getattr(PredefinedImages, image)
    assert test_image.name == name
    assert test_image.tag == tag
    assert test_image.entrypoint == entrypoint
