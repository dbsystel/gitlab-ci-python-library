import pytest

from gcip import Retry


def test_valid_value_rage():
    Retry(max=0)
    Retry(max=1)


def test_rety_lower_bound():
    with pytest.raises(
        AssertionError, match="^The maximum number of retries cannot be negative.$"
    ):
        Retry(max=-1)


def test_rety_upper_bound():
    with pytest.raises(
        AssertionError,
        match="^As of the Gitlab CI documentation in 2024 the maximum number of retries is 2.$",
    ):
        Retry(max=3)
