from gcip import Job, JobFilter, Sequence


def test_find_jobs():
    job1 = Job(stage="test", script="testvalue1")
    job2 = Job(stage="test", script="testvalue2")
    job3 = Job(stage="test", script="testvalue3")
    job4 = Job(stage="test", script="testvalue4")
    job5 = Job(stage="test", script="testvalue5")

    sequence1 = Sequence().add_children(job1)
    sequence2 = Sequence().add_children(sequence1).add_tags("tag1")
    sequence3 = Sequence().add_children(job2, sequence2)
    sequence4 = Sequence().add_children(sequence3, sequence2).add_tags(("tag2"))

    sequence5 = Sequence().add_children(job3, job4).add_tags("tag3")

    sequence6 = Sequence().add_children(job5, sequence4, sequence5)

    assert {job1, job2, job3, job4, job5} == sequence6.find_jobs(
        JobFilter(script="testvalue.")
    )
    assert {job1} == sequence6.find_jobs(JobFilter(script="testvalue1"))
    assert {job1, job3} == sequence6.find_jobs(
        JobFilter(script="testvalue1"), JobFilter(script="testvalue3")
    )
    assert set() == sequence6.find_jobs(JobFilter(script="testvalueX"))

    assert set() == sequence6.find_jobs(JobFilter(tags="tag1"))
    assert {job1} == sequence6.find_jobs(
        JobFilter(tags="tag1"), include_sequence_attributes=True
    )
    assert (
        sequence6.find_jobs(JobFilter(tags="tag1"), include_sequence_attributes=True)
        .pop()
        ._tags
        == {}
    )

    assert set() == sequence6.find_jobs(JobFilter(tags="tag2"))
    assert {job1, job2} == sequence6.find_jobs(
        JobFilter(tags="tag2"), include_sequence_attributes=True
    )

    assert set() == sequence6.find_jobs(JobFilter(tags="tag3"))
    assert {job3, job4} == sequence6.find_jobs(
        JobFilter(tags="tag3"), include_sequence_attributes=True
    )


def test_different_populated_attributes():
    job1 = Job(stage="test", script="testvalue1")
    sequence1 = Sequence().add_children(job1)
    sequence2 = Sequence().add_children(job1).add_tags("tag1")

    sequence3 = Sequence().add_children(sequence1, sequence2)

    assert sequence1._children[0]["child"].artifacts.public is None
    assert sequence2._children[0]["child"].artifacts.public is None

    # Both sequences contain the same job object 'job1'. If 'job1' is found and modified by an attribute
    # of sequence2, the same job object for sequence1 is modified too
    sequence3.find_jobs(
        JobFilter(tags="tag1"), include_sequence_attributes=True
    ).pop().artifacts.public = False

    assert sequence1._children[0]["child"].artifacts.public is False
    assert sequence2._children[0]["child"].artifacts.public is False
