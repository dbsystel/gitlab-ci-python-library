import pytest

from gcip.core.artifacts import Artifacts, ArtifactsReport
from gcip.core.variables import PredefinedVariables
from gcip.core.when import WhenStatement
from tests import conftest


def test_simple_artifacts():
    artifacts = Artifacts("test/artifact/one.txt")
    conftest.check(artifacts.render())  # type:ignore


def test_advanced_artifacts():
    artifacts = Artifacts(
        "a/b/c/d",
        "foo/bar/baz.txt",
        excludes=["*.log"],
        public=False,
        untracked=True,
        expose_as="Artifact 1",
        expire_in="1 month",
        name="Custom Artifact",
        when=WhenStatement.ON_SUCCESS,
    )
    conftest.check(artifacts.render())  # type:ignore


def test_artifacts_report():
    artifacts = Artifacts(reports={ArtifactsReport.COBERTURA: "cov.xml"})
    conftest.check(artifacts.render())  # type:ignore


def test_artifacts_and_report():
    artifacts = Artifacts(
        "foo/bar/baz",
        excludes=["foo/bar/baz/**"],
        reports={ArtifactsReport.CODEQUALITY: "code_quality.xml"},
    )
    conftest.check(artifacts.render())  # type:ignore


def test_exception_artifact():
    with pytest.raises(ValueError):
        Artifacts("/foobar")
    with pytest.raises(ValueError):
        Artifacts("bar/baz/", excludes=["/value/error"])
    with pytest.raises(ValueError):
        Artifacts("bar/baz", when=WhenStatement.DELAYED)


def test_path_is_ci_project_dir():
    """
    https://gitlab.com/dbsystel/gitlab-ci-python-library/-/issues/85
    `PredefinedVariables.CI_PROJECT_DIR` was not properly shortened to '.'
    when path only consists of `PredefinedVariables.CI_PROJECT_DIR`.
    """
    assert Artifacts(PredefinedVariables.CI_PROJECT_DIR).paths[0] == "."
