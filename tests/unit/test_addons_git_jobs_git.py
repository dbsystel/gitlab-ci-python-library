from gcip.addons.git.jobs import git
from tests import conftest


def test_mirror_job(pipeline):
    pipeline.add_children(
        git.Mirror(
            remote_repository="git@myrepo.com:company/gitlab-ci-python-library.git"
        )
    )
    conftest.check(pipeline.render())


def test_mirror_job_configured(pipeline):
    pipeline.add_children(
        git.Mirror(
            remote_repository="git@myrepo.com:company/gitlab-ci-python-library.git",
            git_config_user_email="max@muster.de",
            git_config_user_name="Max Power",
            private_key_variable="TAKE_THAT",
            run_only_for_repository_url="https://mycompay.net/gcip.git",
            script_hook=["a", "b"],
        )
    )
    conftest.check(pipeline.render())
