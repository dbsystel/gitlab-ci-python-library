from gcip.addons.gitlab.jobs.tags import (
    IncrementGitlabTags,
    SemVerIncrementType,
)
from tests import conftest


def test_increment_tags(pipeline):
    pipeline.add_children(
        IncrementGitlabTags(gitlabProjects="foobar", privateTokenEnv="BOING")
    )
    conftest.check(pipeline.render())


def test_increment_tags_configured(pipeline):
    pipeline.add_children(
        IncrementGitlabTags(
            gitlabProjects="foobar",
            incrementType=SemVerIncrementType.PATCH,
            gitlabHost="https://my.git.lab",
            privateTokenEnv="TOKEN_HERE",
            tagMessage="""A multi
line message with
${vars}
inside.
""",
        )
    )
    conftest.check(pipeline.render())


def test_increment_tags_job_token(pipeline):
    pipeline.add_children(
        IncrementGitlabTags(
            gitlabProjects="foobar", privateTokenEnv="BINGO", jobTokenEnv="MY_JOB_TOKEN"
        )
    )
    conftest.check(pipeline.render())
