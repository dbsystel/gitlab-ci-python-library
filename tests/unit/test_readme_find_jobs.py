from gcip import Job, JobFilter, Pipeline
from tests import conftest


def test():
    dev_job = Job(stage="build-dev", script="do_something development")
    prd_job = Job(stage="build-prd", script="do_something production")

    dev_job.set_image("foo/bar:latest")
    prd_job.set_image("foo/bar:latest")

    dev_job.add_tags("dev")
    prd_job.add_tags("prd")

    pipeline = Pipeline()
    pipeline.add_children(dev_job, prd_job)

    # Imagine the upper pipeline is far more complex then what you see.
    # Then the interesting part starts here:

    filter = JobFilter(image="foo/bar:.*", tags="prd")

    for job in pipeline.find_jobs(filter):
        job.set_image("foo/bar:stable")

    conftest.check(pipeline.render())
