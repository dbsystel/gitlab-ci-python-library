import gcip
from gcip.addons.python.sequences import FullStack
from tests import conftest


def test():
    pipeline = gcip.Pipeline()

    pipeline.add_children(
        FullStack(
            twine_dev_repository_url="https://my.artifactory.net/pypi/dev-repository",
            twine_dev_username_env_var="ARTIFACTORY_DEV_USER",
            twine_dev_password_env_var="ARTIFACTORY_DEV_PASSWORD",
            twine_stable_repository_url="https://my.artifactory.net/pypi/prod-repository",
            twine_stable_username_env_var="ARTIFACTORY_PROD_USER",
            twine_stable_password_env_var="ARTIFACTORY_PROD_PASSWORD",
        )
    )

    conftest.check(pipeline.render())


def test_with_mypy():
    pipeline = gcip.Pipeline()
    pipeline.add_children(
        FullStack(
            twine_dev_repository_url="https://my.artifactory.net/pypi/dev-repository",
            twine_dev_username_env_var="ARTIFACTORY_DEV_USER",
            twine_dev_password_env_var="ARTIFACTORY_DEV_PASSWORD",
            twine_stable_repository_url="https://my.artifactory.net/pypi/prod-repository",
            twine_stable_username_env_var="ARTIFACTORY_PROD_USER",
            twine_stable_password_env_var="ARTIFACTORY_PROD_PASSWORD",
            mypy_package_dir="test_package_dir",
        )
    )
    conftest.check(pipeline.render())


def test_with_custom_evaluate_git_tag_pep440_conformity_image():
    pipeline = gcip.Pipeline()
    s = FullStack(
        twine_dev_repository_url="https://my.artifactory.net/pypi/dev-repository",
        twine_dev_username_env_var="ARTIFACTORY_DEV_USER",
        twine_dev_password_env_var="ARTIFACTORY_DEV_PASSWORD",
        twine_stable_repository_url="https://my.artifactory.net/pypi/prod-repository",
        twine_stable_username_env_var="ARTIFACTORY_PROD_USER",
        twine_stable_password_env_var="ARTIFACTORY_PROD_PASSWORD",
        mypy_package_dir="test_package_dir",
    )
    s.evaluate_git_tag_pep404_conformity_job.set_image("custom/image:1.2.3")
    s.mypy_job.mypy_version = "1.2.3"
    pipeline.add_children(s)
    conftest.check(pipeline.render())
