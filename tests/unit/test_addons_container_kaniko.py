from gcip import Pipeline
from gcip.addons.container.config import DockerClientConfig
from gcip.addons.container.jobs import kaniko
from gcip.addons.container.registries import Registry
from tests import conftest


def test_simple_kaniko_job():
    pipeline = Pipeline()
    pipeline.add_children(
        kaniko.Execute(),
        name="simple",
    )
    conftest.check(pipeline.render())


def test_default_kaniko_job():
    pipeline = Pipeline()
    dcc = DockerClientConfig()
    dcc.add_auth(
        "index.docker.io",
        username_env_var="DOCKER_USER",
        password_env_var="DOCKER_LOGIN",
    )

    pipeline.add_children(
        kaniko.Execute(
            image_name="thomass/gcip",
            enable_push=True,
            docker_client_config=dcc,
        ),
        name="gcip",
    )

    conftest.check(pipeline.render())


def test_container_kaniko_job_docker_v2_replacement_():
    pipeline = Pipeline()
    dcc = DockerClientConfig()
    dcc.add_auth(Registry.DOCKER)
    pipeline.add_children(
        kaniko.Execute(
            image_name="thomass/gcip",
            image_tag="v2.2.2",
            docker_client_config=dcc,
        ),
        name="gcip2",
    )

    conftest.check(pipeline.render())


def test_container_kaniko_job_tar_path():
    pipeline = Pipeline()
    pipeline.add_children(kaniko.Execute(tar_path="tar/path/for/image"))
    conftest.check(pipeline.render())
