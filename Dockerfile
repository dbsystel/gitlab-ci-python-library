# Start creating the virtual environment with pyenv as described here:
# https://pipenv.pypa.io/en/latest/basics/#pipenv-and-docker-conatiners

FROM python:3.11-alpine AS builder
RUN pip install --no-cache-dir --user pipenv==2022.08.15
# Tell pipenv to create venv in the current directory
ENV PIPENV_VENV_IN_PROJECT=1
# Pipefile contains requests
ADD Pipfile.lock Pipfile /usr/src/
WORKDIR /usr/src

# Install git and add git directory to make `setuptools-git-versioning` work.
# See: https://setuptools-git-versioning.readthedocs.io/en/latest/index.html
# Also add requirements for further python packages (gcc, musl-dev)
RUN apk  update && \
    apk upgrade && \
    apk add git gcc musl-dev && \
    rm -rf /var/cache/apk/*
ADD .git /usr/src/.git

RUN /root/.local/bin/pipenv sync --dev

# Add all sources required for the build and build the python package
ADD gcip /usr/src/gcip
ADD setup.py /usr/src
ADD README.md /usr/src
RUN /usr/src/.venv/bin/python setup.py bdist_wheel

#
# The final image build startes here:
#

FROM python:3.11-alpine AS runtime

# sacrifice reproducability for security
# and install latest patches
RUN apk  update && \
    apk upgrade && \
    rm -rf /var/cache/apk/*

WORKDIR /usr/src/app

# add metadata and additional files
COPY docker docker
COPY CHANGELOG.md .
COPY LICENCE .
COPY README.md .

# gather gcip python package from previous docker build stage
# and install this package, including further requirements
COPY --from=builder /usr/src/dist /usr/src/app/dist
RUN pip install --no-cache-dir --user pipenv && pip install --no-cache-dir --user dist/*.whl

WORKDIR /workdir

CMD /usr/src/app/docker/gcip.sh
