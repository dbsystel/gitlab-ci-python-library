# Thanks for your interest in our project

Contributions are welcome. Feel free to [open an issue])(issues) with questions or reporting ideas and bugs,
or [open pull requests](pulls) to contribute code.

Currently this project is an early implementation of an idea with a lot of influences from the daily
use in our pipelines at deutschebahn.com. Not yet ready designed we might still publish breaking changes.

We also have a strong opinion of how this library should be designed. That means not all contributions
would be accepted as-is. Please respect if we enforce changes to contributions or deny some. However
our opinion of how things work are changing, so discussions are welcomed.

We are committed to fostering a welcoming, respectful, and harassment-free environment. Be kind!

## Development Hints

We use following tooling for development:

* [pyenv](https://github.com/pyenv/pyenv) for managing local Python installations.
* [pipenv](https://pipenv.pypa.io/) for managing virtual Python environments.

Within the virtual environment we have following tools installed:

* [setuptools-git-versioning](https://setuptools-git-versioning.readthedocs.io/en/latest/index.html) for automatically tagging build packages with git tags.

## Setup development environment

As first one time task you should install the required Python version:

* Look for the required `python_version` in the [Pipfile](Pipfile)
* List available Python versions and install the appropriate one:

```bash
pyenv install --list
pyenv install 3.7.12
```

The second one time task is to create your virtual Python environment with:

```bash
pipenv install --dev
pipenv shell # call this each time you need a shell in the venv
```

When using Visual Studio Code you switch to that virtual environment:

* use `pipenv --venv` to display the name of the venv
* in VSCode type Cmd/Alt+Shift+p and then search/select `Python: Select Interpreter` and finally search and select the venv.

Feel free to use any other editor you like, as long as you will ensure following quality requirements before contributing your commits:

* Organizing and sorting imports with `isort`.
* Formatting code with `black`.
* Type checking with `mypy`.
* Unit testing with `pytest`.

The VSCode settings delivered will do most of that automatically (organize imports, formatting, type checking) or setup the required tools (unit testing with code coverage).

## Further readings

* Please read the [Developer Documentation](https://dbsystel.gitlab.io/gitlab-ci-python-library/developer/index.html).
* Please get yourself into the code to understand conventions within this library.
